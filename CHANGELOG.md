## v2.0.0 / 2019-01-20

* Replace [Bracket Pair Colorizer](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer) by [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)
    * Differences between v1 and v2?
        * v2 Uses the same bracket parsing engine as VSCode, greatly increasing speed and accuracy. A new version was released because settings were cleaned up, breaking backwards compatibility.



## v1.1.0 / 2018-03-24

- Add: section to advise setting VSCode configuration
- Upd: improve the readme

## v1.0.0 / 2018-03-20

- Add: icon
- Add: screenshots with / without
- Fix: typo in README

## 0.1.1 / 2018-03-19

- Upd: improve README's readability

## 0.1.0 / 2018-03-19

- Initial release
