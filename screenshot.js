const getName = ({ user: name }) => { 
	const [firstName, ...lastName] = name.split(' ')​
	return {
		firstName,
		lastName,
	}    
};
