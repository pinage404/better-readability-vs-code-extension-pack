# Better Readability VS Code Extension Pack

Ever wanted to be a superhero? You will see the invisible

Collection of extensions to improve readability



## Screenshot

### With

![screenshot with][screenshot_with]


### Without

![screenshot without][screenshot_without]



## Extensions Included

### Bracket Pair Colorizer

[![Latest Release][bracket-pair-colorizer-version-bagde]][bracket-pair-colorizer-url]
[![Downloads][bracket-pair-colorizer-download-badge]][bracket-pair-colorizer-url]
[![Rating][bracket-pair-colorizer-rating-bagde]][bracket-pair-colorizer-url]

A customizable extension for colorizing matching brackets


### indent-rainbow

[![Latest Release][indent-rainbow-version-bagde]][indent-rainbow-url]
[![Downloads][indent-rainbow-download-badge]][indent-rainbow-url]
[![Rating][indent-rainbow-rating-bagde]][indent-rainbow-url]

Makes indentation easier to read


### Gremlins

[![Latest Release][gremlins-version-bagde]][gremlins-url]
[![Downloads][gremlins-download-badge]][gremlins-url]
[![Rating][gremlins-rating-bagde]][gremlins-url]

Reveals some characters that can be harmful because they are invisible or looking like legitimate ones


### Trailing Spaces

[![Latest Release][trailing-spaces-version-bagde]][trailing-spaces-url]
[![Downloads][trailing-spaces-download-badge]][trailing-spaces-url]
[![Rating][trailing-spaces-rating-bagde]][trailing-spaces-url]

Highlight trailing spaces and delete them in a flash



## VS Code Config

In addition to this extension, it is recommended to display all white spaces by setting the `editor.renderWhitespace` VSCode configuration to `all`

```json
{
	"editor.renderWhitespace": "all",
}
```



## Want to see more extension added?

Open a [MR][merge-request-url] or an [issue][issue-url] and i will to take a look



[screenshot_with]:    https://gitlab.com/pinage404/better-readability-vs-code-extension-pack/raw/master/screenshot_with.png
[screenshot_without]: https://gitlab.com/pinage404/better-readability-vs-code-extension-pack/raw/master/screenshot_without.png

[bracket-pair-colorizer-url]:            https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer
[bracket-pair-colorizer-version-bagde]:  https://img.shields.io/vscode-marketplace/v/CoenraadS.bracket-pair-colorizer.svg
[bracket-pair-colorizer-download-badge]: https://img.shields.io/vscode-marketplace/d/CoenraadS.bracket-pair-colorizer.svg
[bracket-pair-colorizer-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/CoenraadS.bracket-pair-colorizer.svg

[indent-rainbow-url]:            https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow
[indent-rainbow-version-bagde]:  https://img.shields.io/vscode-marketplace/v/oderwat.indent-rainbow.svg
[indent-rainbow-download-badge]: https://img.shields.io/vscode-marketplace/d/oderwat.indent-rainbow.svg
[indent-rainbow-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/oderwat.indent-rainbow.svg

[gremlins-url]:            https://marketplace.visualstudio.com/items?itemName=nhoizey.gremlins
[gremlins-version-bagde]:  https://img.shields.io/vscode-marketplace/v/nhoizey.gremlins.svg
[gremlins-download-badge]: https://img.shields.io/vscode-marketplace/d/nhoizey.gremlins.svg
[gremlins-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/nhoizey.gremlins.svg

[trailing-spaces-url]:            https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces
[trailing-spaces-version-bagde]:  https://img.shields.io/vscode-marketplace/v/shardulm94.trailing-spaces.svg
[trailing-spaces-download-badge]: https://img.shields.io/vscode-marketplace/d/shardulm94.trailing-spaces.svg
[trailing-spaces-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/shardulm94.trailing-spaces.svg

[merge-request-url]: https://gitlab.com/pinage404/better-readability-vs-code-extension-pack/merge_requests
[issue-url]:         https://gitlab.com/pinage404/better-readability-vs-code-extension-pack/issues
